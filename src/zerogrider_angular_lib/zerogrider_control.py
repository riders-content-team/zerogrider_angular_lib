import rospy
from geometry_msgs.msg import Wrench
from std_msgs.msg import Bool 
from math import pi

class RobotControl:

    def __init__(self,topic_name,node_name):
        self.topic_name = topic_name
        self.node_name = node_name

        rospy.init_node(node_name, anonymous=True)
        self.pub = rospy.Publisher(topic_name,Wrench,queue_size=1)
        self.msg = Wrench()

        self.dock_sub = rospy.Subscriber("dock",Bool,self.dockingHandler)
        self.dock_state = False

        self.contact_sub = rospy.Subscriber("contact",Bool,self.contactHandler)
        self.contact_state = False

    def contactHandler(self,contact):
        if contact.data:
            self.contact_state = True

    def dockingHandler(self,dock):
        if dock.data:
            self.dock_state = True

    def up(self,force):
        #todo
        force = force*2*pi/360
        self.msg.force.x = 0
        self.msg.force.y = 0
        self.msg.force.z = force
        self.pub.publish(self.msg)

    def down(self,force):
        force = force*2*pi/360
        self.msg.force.x = 0
        self.msg.force.y = 0
        self.msg.force.z = -force
        self.pub.publish(self.msg)
        
    def left(self, force):
        force = force*2*pi/360
        self.msg.force.x = -force
        self.msg.force.y = 0
        self.msg.force.z = 0
        self.pub.publish(self.msg)

    def right(self, force):
        force = force*2*pi/360
        self.msg.force.x = force
        self.msg.force.y = 0
        self.msg.force.z = 0
        self.pub.publish(self.msg)

    def forward(self, force):
        force = force*2*pi/360
        self.msg.force.x = 0
        self.msg.force.y = force
        self.msg.force.z = 0
        self.pub.publish(self.msg)

    def backward(self, force):
        force = force*2*pi/360
        self.msg.force.x = 0
        self.msg.force.y = -force
        self.msg.force.z = 0
        self.pub.publish(self.msg)
    
    def pitch(self, torque):
        torque = torque*2*pi/360
        self.msg.torque.x = torque
        self.msg.torque.y = 0
        self.msg.torque.z = 0
        self.pub.publish(self.msg)
    
    def roll(self, torque):
        torque = torque*2*pi/360
        self.msg.torque.x = 0
        self.msg.torque.y = torque
        self.msg.torque.z = 0
        self.pub.publish(self.msg)

    def yaw(self, torque):
        torque = torque*2*pi/360
        self.msg.torque.x = 0
        self.msg.torque.y = 0
        self.msg.torque.z = torque
        self.pub.publish(self.msg)
    
    def is_ok(self): 
        if not rospy.is_shutdown() and not self.dock_state and not self.contact_state:
            return True         
        else:         
            return False