#!/usr/bin/env python2
from zerogrider_angular_lib.zerogrider_control import RobotControl
import rospy


node_name = 'test_node'
topic_name = '/robot_angular'
robot = RobotControl(topic_name,node_name)
count = 1
while robot.is_ok():
    if count <= 4:
        robot.roll(100)
        count = count +1
    robot.forward(200)
    robot.left(10)

